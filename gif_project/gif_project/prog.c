/*********************************
* Class: MAGSHIMIM C2			 *
* openCV template                *
* name: Ori Lev                  *
**********************************/

#define CV_IGNORE_DEBUG_BUILD_GUARD
#define _CRT_SECURE_NO_WARNINGS

#include <opencv2/imgcodecs/imgcodecs_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "linkedList.h"
#include "video_edit.h"
#include "view.h"
#include "file_management.h"


#define EXIT 0
#define ADD 1
#define REMOVE 2
#define CHNAGE_INDEX 3
#define CHANGE_DURATION 4
#define CHANGE_ALL_DURATION 5
#define PRINT_LIST 6
#define PLAY 7
#define SAVE 8
#define REVERSE 9


/*
* function print the menu
* input: none
* output: none
*/
void print_menu()
{
	printf("\nWhat would you like to do ?\n");
	printf("[0] Exit\n");
	printf("[1] Add new Frame\n");
	printf("[2] Remove a Frame\n");
	printf("[3] Change frame index\n");
	printf("[4] Change frame duration\n");
	printf("[5] Change duration of all frames\n");
	printf("[6] List frames\n");
	printf("[7] Play movie!\n");
	printf("[8] Save project\n");
	printf("[9] Reverse the list\n");
}


int main()
{
	int choice = 0;
	FrameNode* head = NULL;
	FrameNode* temp = NULL;
	
	head = get_head();

	do
	{
		print_menu();
		scanf("%d", &choice);
		getchar();

		switch(choice)
		{
		case EXIT:
			head = let_her_go(head);
			break;

		case ADD:

			temp = init_node_data();

			if (!isFileExist(temp->frame->path)) { }
			else if(name_exist_in_list(head, temp->frame->name))
			{
				printf("Name already taken, try another name!\n");
			}
			else
			{
				head = push_Node(head, temp);
			}
			break;

		case REMOVE:
			head = deleteNode(head);
			break;
		
		case CHNAGE_INDEX:
			head = change_frame_place(head);
			break;
		case CHANGE_DURATION:
			change_duration_of_frame(head);
			break;

		case CHANGE_ALL_DURATION:
			change_duration_of_all_frames(head);
			break;
		case PRINT_LIST:
			viewAllFrames(head);
			break;

		case PLAY:
			play(head);
			break;

		case SAVE:
			save_file(head);
			break;

		case REVERSE:
			head = reversedLinkedList(head);
			break;

		default:
			printf("Invalid input!\n");
			break;
		}

	} while (choice!=EXIT);
	
	
	getchar();
	return 0;
}
