#include "video_edit.h"


/*  The function get str, and size of str, and get value to the str
	input: str, size of str
	output: None*/
void myFgets(char* str, int size)
{
	fgets(str, size, stdin);
	str[strcspn(str, "\n")] = '\0';
}


/*
* function get path of file, and check if the file exist
* input: a path of file
* output: 1 - exist, 0 - dosn't exist
*/
int isFileExist(char* path)
{
	FILE* temp = NULL;
	
	temp = fopen(path, "r");

	if (temp != NULL)
	{
		fclose(temp);
		return TRUE;
	}
	printf("file dosn't exist!\n");
	return FALSE;
}


/*
* function print all the frames
* input: head of linked list frame
* output: none
*/
void viewAllFrames(FrameNode* head)
{
	while (head != NULL)
	{
		printf("duration: %d\n", head->frame->duration);
		printf("name: %s\n", head->frame->name);
		printf("path: %s\n\n", head->frame->path);
		head = head->next;
	}

}


/*
* function change the duration of all the frame's
* input: a head of the list
* output: none
*/
void change_duration_of_all_frames(FrameNode* head)
{
	int newDuration = 0;

	printf("Enter new duration: ");
	scanf("%d", &newDuration);
	getchar();

	while (head != NULL)
	{
		head->frame->duration = newDuration;
		head = head->next;
	}
}


/*
* function check if some name exist in the list
* input: a linked list, and name to trace
* output: 1 - name exist, 0 - not exist
*/
int name_exist_in_list(FrameNode* head, char* name)
{
	while (head != NULL)
	{
		if (strcmp(head->frame->name, name) == 0)
		{

			return TRUE;
		}
		head = head->next;
	}
	return FALSE;
}



/*
* function push new node to the end of the list
* input: a head of list, and new node
* output: the new head of the list
*/
FrameNode* push_Node(FrameNode* head, FrameNode* newNode)
{
	FrameNode* temp = head;
	if (head == NULL)
	{
		return newNode;
	}
	while (head->next != NULL)
	{
		head = head->next;
	}
	head->next = newNode;
	return temp;
}


/*
* function get a list, and name of item to delete, and remove the item from the list
* input: head of list
* output: the new head of the list
*/
FrameNode* deleteNode(FrameNode* head)
{
	FrameNode* temp = NULL;
	FrameNode* after = NULL;
	char name[MAX] = { 0 };

	printf("Enter node name: ");
	myFgets(name, MAX);

	if (head == NULL)
	{
		printf("The list is empty, can't delete frame!\n");
		return head;
	}

	if (strcmp(head->frame->name, name) == 0)
	{
		temp = head->next;
		free(head);
		return temp;
	}

	temp = head;
	while (head->next != NULL)
	{
		if (strcmp(head->next->frame->name, name) == 0)
		{
			after = head->next->next;
			free(head->next->frame);
			free(head->next);
			head->next = after;
			return temp;
		}
		head = head->next;
	}
	printf("No find node with the name: %s\n", name);
	return temp;
}


/*
* functoin get a list, name of item and new duration, and set the duration of the frame
* input: head of list
* output: None
*/
void change_duration_of_frame(FrameNode* head)
{
	int new_duration = 0;
	int index = 0;
	int counter = 0;

	printf("Enter new duration: ");
	scanf("%d", &new_duration);
	getchar();

	printf("Enter index: ");
	scanf("%d", &index);
	getchar();


	while ((head != NULL) && (counter < index - 1))
	{
		counter++;
		head = head->next;
	}

	if (head != NULL)
	{
		head->frame->duration = new_duration;
		return;
	}
}


/*
* function find the length of the list
* input: a head of list
* output: the length of the list
*/
int get_length(FrameNode* head)
{
	int counter = 0;
	while (head != NULL)
	{
		counter++;
		head = head->next;
	}
	return counter;
}


/*
* function find a node by his name, and push him to place k in the list
* input: head of list
* output: the new head of the list
*/
FrameNode* change_frame_place(FrameNode* head)
{
	int k = 0;
	char name[MAX] = { 0 };
	int flag = 1, i = 0;
	FrameNode* temp = NULL;
	FrameNode* copy_head = head;

	printf("Enter the name of the frame: ");
	myFgets(name, MAX);

	printf("Enter the new index in the movie you wish to place the frame: ");
	scanf("%d", &k);
	getchar();


	if ((k > get_length(head)) || (! name_exist_in_list(head, name)))
	{
		printf("Invalid index or name, try again!\n");
		return head;
	}
	
	
	if ((head == NULL) || (head->next == NULL))
	{
		return head;
	}

	// find the Node
	if (strcmp(head->frame->name, name) == 0)
	{
		copy_head = head->next;
		temp = head;
		temp->next = NULL;
		head = copy_head;
	}
	else
	{
		while ((head->next != NULL) && flag)
		{
			if (strcmp(head->next->frame->name, name) == 0)
			{
				temp = head->next;
				head->next = temp->next;
				temp->next = NULL;
				flag = 0;
			}
			else
			{
				head = head->next;
			}
		}
	}
	head = copy_head;
	
	// push the node

	if (k == 1)
	{
		temp->next = head;
		return temp;
	}
	else
	{
		for (i = 0; i < k - 2; ++i)
		{
			head = head->next;
		}
		temp->next = head->next;
		head->next = temp;
	}
	return copy_head;
}


/*
* function create new node
* input: None
* output: new node
*/
FrameNode* createNode(char* name, char* path, int duration)
{
	Frame* temp_frame = (Frame*)malloc(sizeof(Frame));
	FrameNode* newNode = (FrameNode*)malloc(sizeof(FrameNode));
	
	temp_frame->duration = duration;
	strncpy(temp_frame->name, name, strlen(name) + 1);
	strncpy(temp_frame->path, path, strlen(path) + 1);

	newNode->frame = temp_frame;
	newNode->next = NULL;
	
	return newNode;
}


/*
* function free the list
* input: a head of list to free
* output: the new head (null)
*/
FrameNode* let_her_go(FrameNode* head)
{
	FrameNode* temp = NULL;

	while (head != NULL)
	{
		temp = head->next;
		free(head->frame);
		free(head);
		head = temp;
	}
	return head;
}


/*
* function use when the user want to create new frame (get data here)
* input: none
* output: new frame node
*/
FrameNode* init_node_data()
{
	char name[MAX] = { 0 };
	char path[MAX] = { 0 };
	int duration = 0;

	printf("Enter name: ");
	myFgets(name, MAX);

	printf("Enter path: ");
	myFgets(path, MAX);

	printf("Enter duration: ");
	scanf("%d", &duration);
	getchar();

	
	return createNode(name, path, duration);
}


/*
* function ask the user if he want to load gif
* input: None
* output: the head of the new gif
*/
FrameNode* get_head()
{
	int choice = 0;
	FrameNode* frame = NULL;
	printf("Welcome to Magshimim Movie Maker!what would you like to do ?\n");
	printf("[0] Create a new project\n");
	printf("[1] Load existing project\n");

	scanf("%d", &choice);
	getchar();

	switch (choice)
	{
	case EMPTY:
		break;
	case LOAD:
		frame = load_file();
		break;
	default:
		printf("Invalid input!\nstart with empty list.\n");
		break;
	}

	return frame;
}


/*  function get linked list, and reverse it
	input: head of linked list to reverse
	output: new head*/
FrameNode* reversedLinkedList(FrameNode* head)
{
	FrameNode* previous = NULL;
	FrameNode* nxt = NULL;

	while (head != NULL)
	{
		nxt = head->next;
		head->next = previous;

		previous = head;
		head = nxt;
	}
	printf("Line reversed!\n\n");
	return previous;
}
