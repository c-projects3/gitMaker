#ifndef LINKEDLISTH
#define LINKEDLISTH

#define CV_IGNORE_DEBUG_BUILD_GUARD


#define FALSE 0
#define TRUE !FALSE
#define SIZE 200

// Frame struct
typedef struct Frame
{
	char name[SIZE];
	unsigned int	duration;
	char path[SIZE];
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;


#endif