#ifndef FILE_MANAGEMENTH
#define FILE_MANAGEMENTH

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "linkedList.h"
#include "video_edit.h"

#define NODES_DELIMETER "\n"
#define PARTS_DELIMETER "ZX***ZX"
#define MAX_NODE 500
/*
* using to create, save, and load saved gif files.
*/

FrameNode* load_file();
void save_file(FrameNode* head);


#endif // !FILE_MANAGEMENTH
