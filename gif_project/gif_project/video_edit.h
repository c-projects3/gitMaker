#ifndef VIDEO_EDITH
#define VIDEO_EDITH

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "linkedList.h"
#include "file_management.h"

#define MAX 200
#define EMPTY 0
#define LOAD 1


void viewAllFrames(FrameNode* head);
void change_duration_of_all_frames(FrameNode* head);
void change_duration_of_frame(FrameNode* head);
void myFgets(char* str, int size);

int isFileExist(char* path);
int name_exist_in_list(FrameNode* head, char* name);
int get_length(FrameNode* head);

FrameNode* free_list(FrameNode* head);
FrameNode* push_Node(FrameNode* head, FrameNode* newNode);
FrameNode* deleteNode(FrameNode* head);
FrameNode* change_frame_place(FrameNode* head);
FrameNode* createNode(char* name, char* path, int duration);
FrameNode* let_her_go(FrameNode* head);
FrameNode* init_node_data();
FrameNode* get_head();
FrameNode* reversedLinkedList(FrameNode* head);



#endif // !VIDEO_EDITH
