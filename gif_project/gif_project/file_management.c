#include "file_management.h"


/*
* function get a file, read it, parse the data and create a new list with the data
* input: none
* output: head of the new list
*/
FrameNode* load_file()
{
	FrameNode* head = NULL;
	FrameNode* node = NULL;

	FILE* f = NULL;

	char path[MAX] = { 0 }; // use to get the path of the file
	
	//using the node
	char temp[MAX_NODE] = { 0 }; // use to get node

	// use to save one part in temp (path\name\duration)
	char* node_name = NULL; 
	char* node_path = NULL;
	char* node_duration = NULL;
	
	
	printf("Enter path of saved file: ");
	myFgets(path, MAX);

	f = fopen(path, "r");
	if (f == NULL)
	{
		printf("Can't open the file, try again!");
		return head;
	}
	
	while (fgets(temp, MAX_NODE, f))
	{
		node_name = strtok(temp, PARTS_DELIMETER);
		node_path = strtok(NULL, PARTS_DELIMETER);
		node_duration = strtok(NULL, PARTS_DELIMETER);
		
		node = createNode(node_name, node_path, atoi(node_duration));
		head = push_Node(head, node);
	}

	fclose(f);
	return head;
}



/*
* function save the file by the format: name <delimeter> path <delimeter> duration <new node delimeter>
* input: head of list to save
* output: none
*/
void save_file(FrameNode* head)
{
	char path[MAX] = { 0 };
	FILE* f = NULL;
	char temp[MAX] = { 0 };


	printf("Enter path of file to save the gif: ");
	myFgets(path, MAX);

	f = fopen(path, "w");
	if (f == NULL)
	{
		printf("error accrue with the file, try again!\n");
		return;
	}

	while (head != NULL)
	{
		fputs(head->frame->name, f);
		fputs(PARTS_DELIMETER, f);

		fputs(head->frame->path, f);
		fputs(PARTS_DELIMETER, f);

		sprintf(temp, "%d", head->frame->duration);
		fputs(temp, f);
		fputs(PARTS_DELIMETER, f);

		if (head->next != NULL)
		{
			fputs(NODES_DELIMETER, f);
		}
		
		head = head->next;
	}

	fclose(f);
}
